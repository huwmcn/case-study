const request = require('supertest');
const app = require('../src/server');

describe('express', () => {

    it('responds to /', (done) => {
        request(app)
            .get('/')
            .expect(200, done);
    });

    it('responds to /health', (done) => {
        request(app)
            .get('/health')
            .expect(200, done);
    });

});