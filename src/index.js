'use strict';
const app = require('./server');

const PORT = 8080;
const HOST = '0.0.0.0';

app.listen(PORT, HOST);
console.log(`Running on ${PORT}`);
