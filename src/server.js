'use strict';
const express = require('express');
const os = require('os');

const app = express();
app.get('/', (req, res) => {
    res.send(`Hello from devops ${os.hostname()}!\n`);
});

app.get('/health', (req, res) => {
    res.send();
});

module.exports = app;
