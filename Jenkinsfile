pipeline {

    agent { label 'docker' }

    environment {
        scannerHome = tool 'sonar'
    }

    options {
        buildDiscarder logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '5')
    }

    stages {

        stage('install') {
            steps {
                sh 'npm install'
            }
        }

        stage('pre-build') {
            parallel {
                stage('test') {
                    steps {
                        sh 'npm run test'
                    }
                }

                stage('lint') {
                    steps {
                        sh 'npm run lint'
                    }
                }

                stage('sonarqube') {
                    steps {
                        withSonarQubeEnv('sonar') {
                            sh "${scannerHome}/bin/sonar-scanner"
                        }
                    }
                }
            }
        }        

        stage("quality gate") {
            steps {
                withSonarQubeEnv('sonar') {
                    timeout(time: 10, unit: 'MINUTES') {
                        waitForQualityGate abortPipeline: true
                    }
                }
            }   
        }
        
        stage('build') {
            steps {
                sh 'docker build -f "Dockerfile" -t huwmcn/case-study:latest .'
            }
        }

        stage('newman') {
            steps {
                sh "docker run -d --name test${BUILD_NUMBER} -p 8999:8080 huwmcn/case-study"
                sh 'npm run newman'
            }
            post {
                always {
                    sh "docker stop test${BUILD_NUMBER} && docker rm test${BUILD_NUMBER}"
                }
            }
        }

        stage('publish') {
            steps {
                withDockerRegistry(registry: [credentialsId: 'dockerhub']) {
                    sh 'docker push huwmcn/case-study:latest'
                }
            }
        }

        stage('notification') {
            steps {
                slackSend color: "good", message: "build ${BUILD_NUMBER} complete ${BUILD_URL}"
            }
        }
    }
    
    post {

        failure {
            slackSend color: "warning", message: "build ${BUILD_NUMBER} failed ${BUILD_URL}"
        }
    }
}
